# ah_osconfig_podman

An Ansible role for installing and configuring Podman.

Also configures `podman.service` to allow management with Cockpit, and `podman.socket` to allow compatibility with Docker Compose.

## Compatibility
Tested for compatibility and idempotence against:

| **Distribution** | **Tested** (Y / N) | **Compatibility** (WORKING / TODO) |
|---|---|---|
| Fedora | Y | WORKING |
| Raspberry Pi OS | Y | WORKING |
| RHEL | Y | WORKING |
| Rocky Linux | Y | WORKING |
| Ubuntu | Y | WORKING |

## Changelog

| **Date** | **Description** |
|---|---|
| 2022-01-12 | Initial commit |
